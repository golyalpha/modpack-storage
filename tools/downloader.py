import requests
import hashlib
import os
import zipfile
from os import path
from sys import argv

solder = "https://fierce-reaches-35013.herokuapp.com"

def get_mods(modpack, build):
    data = requests.get(f"{solder}/api/modpack/{modpack}/{build}").json()
    try:
        os.mkdir(".cache")
    except:
        print("Cache already in workdir.")
    for mod in data["mods"]:
        cachepath = path.join(".cache", f"{mod['name']}-{mod['version']}.zip")
        if path.exists(cachepath):
            with open(cachepath, "rb") as pkg:
                if hashlib.md5(pkg.read()).hexdigest() == mod["md5"]:
                    continue
        with open(cachepath, "wb") as pkg:
            pkg.write(
                requests.get(mod["url"]).content
            )
    for mod in data["mods"]:
        cachepath = path.join(".cache", f"{mod['name']}-{mod['version']}.zip")
        with zipfile.ZipFile(cachepath) as pkg:
            pkg.extractall()


def get_latest(modpack):
    return requests.get(f"{solder}/api/modpack/{modpack}").json()["latest"]


def get_recommended(modpack):
    return requests.get(f"{solder}/api/modpack/{modpack}").json()["recommended"]

try:
    modpack = argv[1]
except IndexError:
    print("You need to specify modpack.")
    print("Usage: script [modpack] <release>")

try:
    release = argv[2]
except IndexError:
    print("Release unspecified, defaulting to recommended.")
    release = get_recommended(modpack)

if release == "latest":
    release = get_latest(modpack)
if release == "recommended":
    release = get_recommended(modpack)

print("Downloading and installing mods.")
get_mods(modpack, release)
print("Done. Forge universal at: bin/modpack.jar")
print("You shouldn't need it if you used the server installer, for the right version of forge and minecraft.")