from zipfile import ZipFile
from tkinter import Tk
from os import getenv, remove, mkdir
from os.path import join, split
from tkinter import filedialog
from json import load as JSONParse
from json.decoder import JSONDecodeError as JSONError
import string

Tk().withdraw()

remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)

modfiles = filedialog.askopenfilenames(
    initialdir=f"{getenv('appdata', default='.')}\\.minecraft\\mods",
    title="Select mods to pack."
    )
modrepo_path = filedialog.askdirectory(title="Select Solder Mod Repo directory")
for mod in modfiles:
    print(f"Loading {mod}.")
    
    with ZipFile(mod) as modjar:
        try:
            modjar.extract("mcmod.info")
        except KeyError:
            pass
    
    try:
        with open("mcmod.info") as modinfo_file:
            try:
                mod_list = JSONParse(modinfo_file)
            except JSONError:
                mod_list = [{}]
            try:
                mod_info = mod_list[0]
            except KeyError:
                mod_info = mod_list["modList"][0]
        remove("mcmod.info")
    except IOError:
        mod_info = {}
    try:
        mod_name = mod_info["modid"].lower().translate(remove_punctuation_map)
        if mod_name == "":
            raise KeyError
    except KeyError:
        mod_name = input("Couldn't find mod name. Manual input: ").lower()
    try:
        mod_mcver = mod_info["mcversion"]
    except KeyError:
        mod_mcver = input("Couldn't find mod MC version. Manual input: ")
    try:
        mod_ver = mod_info["version"]
    except KeyError:
        mod_ver = input("Couldn't find mod version. Manual input: ")
    slug = f"{mod_name}/{mod_name}-{mod_mcver}-{mod_ver}.zip"
    
    print("Found and parsed mcmod.info")
    print(f"Autoconstructed slug: {slug}")
    
    try:
        mkdir(join(modrepo_path, "mods"))
    except OSError:
        pass
    try:
        mkdir(join(modrepo_path, "mods", split(slug)[0]))
    except OSError:
        pass

    with ZipFile(join(modrepo_path, "mods", slug), mode="w") as mod_package:
        mod_package.write(mod, f"mods/{split(mod)[1]}")
